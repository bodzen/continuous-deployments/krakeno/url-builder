#!/bin/bash
# Randomly generate & create a credentials to connect to core-database
# Rights sets to: Read + Write

set -e
source $OUTPUT_FUNC

export SECRET_NAME='url-builder-postgres-secret'
MANIFEST_FILE="${MANIFEST_PATH}/postgres_secret.yaml"

if secret_already_exist
then
	print_secret_already_exist
else
	print_missing_secret
	sed -i 's|POSTGRES_URI_B64|'"$POSTGRES_URI_B64"'|g' $MANIFEST_FILE
	unset NEW_POSTGRES_SECRET POSTGRES_SECRET_B64
	kubectl -n $GIVEN_NAMESPACE apply -f $MANIFEST_FILE
	rm -f $MANIFEST_FILE

fi
