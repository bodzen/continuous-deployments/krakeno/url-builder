CREATE USER url_builder_user WITH encrypted password 'URL_BUILDER_USER_PSQL_PASS';

/* Grant privileges for existing tables */
GRANT SELECT, INSERT, UPDATE, DELETE
ON ALL TABLES IN SCHEMA public
TO url_builder_user;

/* Create related role with limited rights */
CREATE ROLE url_builder_role WITH LOGIN;
GRANT url_builder_role to zeus;
GRANT url_builder_role to url_builder_user;

/* Grant privileges for all future table create */
ALTER DEFAULT PRIVILEGES
	  FOR ROLE url_builder_role
	  IN SCHEMA public
	  GRANT SELECT, INSERT, UPDATE, DELETE ON TABLES TO url_builder_user;

/* Grant privilegies on sequence created by Alembic */
GRANT UPDATE ON SEQUENCE yt_headers_id_seq TO url_builder_user;
