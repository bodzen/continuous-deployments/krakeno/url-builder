# Url-Builder Helm Chart

Continuous deployement for the project [Url-Builder](https://gitlab.com/bodzen/krakeno/url-builder)

## Requirements

- helm 3
- kubectl >= v1.16.3
